### Supplementary Video 1

![Supplementary Video 1](Video S1.mp4)

Neural network predictions of phase maps from voltage-sensitive optical mapping video data. The recording shows action potential spiral vortex waves during ventricular fibrillation on the left ventricular surface of a porcine heart, see also Fig. 6. The pixel-wise normalized transmembrane voltage is shown on the left (yellow: depolarized, blue: repolarized tissue). Center: smoothed ground truth phase map, which was obtained from the noisy optical maps using the Hilbert transform, see Fig. 5A) and section 2.2. Right: phase map predicted by the neural network.

### Supplementary Video 2

![Supplementary Video 2](Video S2.mp4)

Comparison of the neural network input images for the prediction of a single phase map for the pig, rabbit, and simulation datasets used in Figs. 7 and 8. The video shows the N_t = 10 images given as input to the neural network to predict a single phase map for each type of dataset.

### Supplementary Video 3

![Supplementary Video 3](Video S3.mp4)


PS prediction for rabbit optical mapping data using neural network models M1 (left), M1A (middle), M1B (right). The PS for model M1 are predicted indirectly by first predicting phase maps then computing PS in the phase maps using the circular line integration method.

### Supplementary Video 4

![Supplementary Video 4](Video S4.mp4)

Phase maps predicted by the neural network from (sparse) simulated electrical excitation wave maps without and with noise (σ = 0.3). Left: electric excitation wave maps uses as network input (N_t = 5). Center: ground truth or true phase. Right: neural network output.

### Supplementary Video 5

![Supplementary Video 5](Video S5.mp4)

Phase singularities (PS) predicted by the neural network M1A from (sparse) simulated electrical spiral wave chaos without and with noise (σ = 0.2) for N_t = 5. Left: ground truth electrical excitation, Center: network input, Right: predicted PS (black) and true PS (white) superimposed onto the corresponding electrical excitation wave maps.

### Supplementary Video 6

![Supplementary Video 6](Video S6.mp4)

Phase maps predicted by the neural network from (sparse) simulated electrical excitation wave maps without and with noise (σ = 0.3) for N_t = 5.

### Supplementary Video 7

![Supplementary Video 7](Video S7.mp4)

Neural network prediction of a single phase map. The 5 noisy and sparse electrical wave frames given as input to the neural network, as well as the predicted phase map and the true phase map are shown.

### Supplementary Video 8

![Supplementary Video 8](Video S8.mp4)

PS prediction using model M1 for the sparse and noisy excitation patterns shown in Fig. 11. The predicted PS are shown in red, the ground truth in white. Left: The sparse excitation wave pattern with noise used as neural network input. Right: Ground truth excitation with sparsification shown in black.